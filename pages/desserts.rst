desserts
========

:ref:`Ade`, :ref:`Bde`, :ref:`Cde`, :ref:`Rde`
----------------------------------------------

.. _Ade:

A
-

:doc:`../2018/11/01/apple_pie`

.. _Bde:

B
-
   
:doc:`../2018/11/01/banana_nut_snacks`

:doc:`../2018/11/01/Black_and_White_Cookies`

.. _Cde:

C
-

:doc:`../2018/11/01/chocolate_chip_cookies`

:doc:`../2018/11/01/crumb_cake`

:doc:`../2018/11/01/crumb_topping`

.. _Rde:

R
-

:doc:`../2018/11/01/reeses_cups`

:doc:`../2018/11/01/roll_cake`
