dinners
=======

:ref:`Cd`, :ref:`Pd`, :ref:`Sd`, :ref:`Td`, :ref:`Vd`, :ref:`Zd`
----------------------------------------------------------------

.. _Cd:

C
--

:doc:`../2018/11/01/chili`

.. _Pd:

P
--

:doc:`../2018/11/01/pasta_with_white_bolognese`

:doc:`../2018/11/01/pesto_pasta`

:doc:`../2018/11/01/pulled_pork`

.. _Sd:

S
--

:doc:`../2018/11/01/Skewered_Chicken_Strips`

.. _Td:

T
--

:doc:`../2018/11/01/tomato_crostini`

.. _Vd:

V
--

:doc:`../2018/11/01/vegetable_salad`

.. _Zd:

Z
--

:doc:`../2018/11/01/zucchini_and_squash_pasta`
