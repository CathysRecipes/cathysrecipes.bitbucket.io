.. banana-bread:

Banana Bread
============

.. author:: Laura
.. categories:: bread
.. tags:: 
.. comments::

=========  =======================================
measure    ingredients
=========  =======================================
3	       eggs
2 cups	   sugar
1 cup      oil
1 Tbsp 	   vanilla extract
3 	       bananas
2 cups     flour
1 Tbsp     cinnamon
2 tsp 	   baking soda
1 tsp 	   salt
1/4 tsp    baking powder
1 cup      walnuts or raisins (optional)
=========  =======================================

Recipe
------

1.  Preheat oven to 350 degrees.  Grease and flower 2 loaf forms.

2.  Beat eggs until fluffy, then beat in sugar, oil, and vanilla.  Beat until thick and lemon colored.

3.  Stir in bananas, beat well.  Add sifted flour, cinnamon, baking soda, salt, and baking powder.

4.  Fold in walnuts/raisins.

5.  Bake 1 hour - cool 10 minutes.

