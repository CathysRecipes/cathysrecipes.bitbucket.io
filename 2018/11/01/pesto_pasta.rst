.. pesto-pasta:

Pesto Pasta
===========

.. author:: Mom
.. categories:: dinners
.. tags:: italian, quick, easy
.. comments::

=========  ===================================================
measure    ingredients
=========  ===================================================
9-oz       package of fresh angel hair, linguine or fettuccine
2 cups     cups fresh basil leaves packed
2 cloves   garlic, minced
1/4 cup    pignolias (pine) nuts
1/2 tsp    salt
1/8 tsp    pepper
1/2 cup    olive oil
1/2 cup    freshly grated Parmesan
3 tbsp     butter, softened
=========  ===================================================

Recipe
------

1. Cook pasta according to package directions and set aside.

2.  Place basil, garlic, nuts, salt, pepper and 2 tbsp of oil in blender and whirl at high speed until basil leaves are roughly chopped.

3.  Slowly add remaining olive oil in a thin stream, with blender at medium speed.

4.  Transfer mixture to a bowl and beat in cheese by hand.

5. Add softened butter and beat by hand. (Beating by hand produces better texture.)

6.  Add a few tbsp of water in which the pasta was cooked (for a thinner consistency) and spoon over hot pasta.


.. note::
   * Pesto sauce may be kept, barely covered in olive oil, in the refrigerator for three months.
   * Don't use all that butter. 1 tbsp is sufficient.
   * Consider making a caprese sandwich!