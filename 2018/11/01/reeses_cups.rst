.. reeses-cookie-cups:

Reese's Cookie Cups
====================

.. author:: Mom
.. categories:: christmas cookies
.. tags:: christmas, cookies, dessert
.. comments::

=========  =======================================
measure    ingredients
=========  =======================================
1 bag	   reese's cups
1  	       chocolate chip cookie dough
--	       mini cup cake wrappers
=========  =======================================

Recipe
------

1.  Fill wrapper 3/4 full with cookie dough.

2.  Bake at 350 degrees until brown.

3.  As soon as cookies come out of the oven, place reese's cup in the center and press down.  Allow cups to cool completely before removing them from baking tray.

