.. sausage-bread:

Sausage Bread
=============

.. author:: Mom
.. categories:: appetizers
.. tags:: quick, easy
.. comments::

=========  =======================================
measure    ingredients
=========  =======================================
1 	       package pillsbury pizza dough
12 slices  munster cheese
1/8 lb     super thin sliced pepperoni
5-6 	   sausage, cooked without casing
1 	       egg
->	       poppy, sesame, or caraway seed
=========  =======================================

Recipe
------

1.  Lay dough on a baking tray.  Lay rows of munster cheese and then pepperoni slices.

2.  Remove sausage from casings and brown before adding on top of pepperoni slices.

3.  Roll together (long way) and flip so that seam is on the bottom.

4.  Coat dough with an egg wash and then sprinkle your choice of seeds on top.   

5.  Bake at 400 degrees until golden brown.

.. note::
   * Refrigerate 1-2 days or freeze.