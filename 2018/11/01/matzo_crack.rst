.. matzo-crack:

Matzo Crack
===========

.. author:: thekitchn.com
.. categories:: desserts
.. tags:: 
.. comments::

This recipe was adapted from `the kitchn <http://www.thekitchn.com/recipe-chocolate-caramel-matzo-47589>`_

==========  ============================================================
measure     ingredients
==========  ============================================================
4-5 pieces  matzo
1 cup       firmly packed dark brown sugar
1 cup       unsalted butter
1 cup       chopped bittersweet chocolate
`or`        semi-sweet chocolate chips toppings, if desired
==========  ============================================================

Recipe
------

1.  Preheat oven to 375 degrees and line a baking sheet with aluminum foil and/or parchment paper.

2.  Place the matzo in one later on the baking sheet, breaking it when necessary to fill the pan completely.  Set aside.

3.  In a large sauce pan, melt the butter and brown sugar over medium heat, stirring constantly.  Once the mixture reaches a boil, continue to cook for an additional 3 minutes, still stirring, until thickened and just starting to pull away from the sides of the pan.  Remove from heat and pour over the matzo, spreading an even layer with a heat proof spatula.

4.  Put the pan in the oven, then immediately turn the heat down to 350 degrees.  Bake for 15 minutes, watching to make sure if doesn't burn.  If it looks like it is starting to burn, turn heat down to 325. (while it is cooking, resist urge to scrape the pan with extra pieces of matzo. you will burn yourself!)

5.  After 15 minutes, the toffee should have bubbled up and turned a rich golden brown.  Remove from the oven and immediately sprinkle the chocolate over the pan.  Let sit for 5 minutes, then spread the now melted chocolate evenly with a spatula.  You can add desired topping at this point.

