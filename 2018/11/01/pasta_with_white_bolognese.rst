.. silk-handkerchief-pasta:

Silk-Handkerchief Pasta with White Bolognese
============================================

.. author:: people magazine
.. categories:: dinners
.. tags:: 
.. comments::

This recipe was adapted from `People Magazine <http://www.people.com/people/archive/article/0,,20615845,00.html>`_

==========  =====================================================
measure     ingredients
==========  =====================================================
2 Tbsp	    olive oil, plus more for garnish
1 lb	    ground pork
1 lb  	    ground beef
1 cup 	    finely chopped onion
1 cup	    finely chopped fennel
1 cup	    finely chopped celery
1 cup 	    finely chopped parsnip
4           cloves garlic, chopped
2 tsp 	    red pepper flakes
2 tsp	    black pepper
1 tsp	    salt
3 1/2 cups  dry white wine (1 bottle)
4 cups	    low sodium chicken stock
1 cup 	    whole milk
1 Tbsp	    chopped sage, plus more for garnish
1 lb        lasagna sheets, broken into thirds
..          grated parmigiano reggiano cheese and chopped parsley
==========  =====================================================

Recipe
------

1.  Heat oil in a very large skillet and brown meats until cooked through.  Remove meat; leave liquid in skillet.

2.  Add vegetables, garlic, pepper flakes, salt, and black pepper to skillet; cook, stirring, until brown, 12-14 minutes.

3.  Return meat to skillet, add wine and reduce until almost evaporated.  Add stock and reduce until almost evaporated.  Add milk and sage; simmer until thick, 10 minutes

4.  Cook lasagna until al dente.  Drain, reserving 1 cup pasta water.

5.  Add pasta and pasta water to skillet and cook 1 minute.

6.  Serve, garnish with olive oil, cheese, sage and parsley.

