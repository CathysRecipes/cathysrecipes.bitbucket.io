.. buffalo-chicken-dip

Buffalo Chicken Dip
===================

.. author:: Mom
.. categories:: appetizers
.. tags:: fan favorite, delicious, heaven
.. comments::

=========  =======================================
measure    ingredients
=========  =======================================
4 	       chicken breasts
1/2 cup    blue cheese
1/2 cup    frank's hot sauce
8 oz       cream cheese
1/2 cup    mozzarella (optional)
=========  =======================================

Recipe
------

1.  Boil 4 chicken breast for 20 minutes.  Allow to drain and cool.

2.  Once cool, shred chicken using grater or food processor, into large bowl.  

3.  Combine blue cheese, hot sauce, cream cheese, and mozzarella (if desired) with shredded chicken.  

4.  Bake in oven at 350 degrees until warmed throughout.
