.. stuffed-potatoes:

Stuffed Potatoes
================

.. author:: Mom
.. categories:: side dishes
.. tags:: 
.. comments::

=========  =======================================
measure    ingredients
=========  =======================================
10         potatoes
1 1/2 tsp  vegetable oil (optional)
1/2 slice  scallions
1/2 cup    sour cream
1 tsp      salt
1/2 tsp    white pepper
1 cup      shredded cheddar
1/4 cup    butter
dash       paprika
=========  =======================================

Recipe
------

1.  Bake potatoes at 400 degrees for 1 hour; until fork easily enters.

2.  Cool to the touch.

3.  Cut in half, long way.

4.  Scoop out potato, leaving small amount of potato in shell and put potato pulp in a bowl.

5.  Sautee onions in butter until tender.  Add to potato pulp.

6.  Add in creams, salt, and pepper.

7.  Beat until smooth, fold in cheese, stuff potato in shells.

8.  Before baking; drizzle with butter and dash of paprika.

9.  Bake at 350 degrees for 20-30 minutes.

.. note::
   * 