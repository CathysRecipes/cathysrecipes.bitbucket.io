.. spinach-and-artichoke-bake:

Spinach and Artichoke Bake
========================== 

.. author:: Mom
.. categories:: appetizers
.. tags:: quick, easy
.. comments::

==========  ==========================================
measure     ingredients
==========  ==========================================
3 pkgs      chopped, frozen spinach defrosted no water
1 jar       artichoke hearts
2 tbls      mayo
6 tbls      milk
8 oz pkg    cream cheese
`to taste`  pepper
..          parmesan cheese
==========  ==========================================

Recipe
------

1.  Mix mayo, pepper, cream cheese and milk.  Whip in blender.

2.  Line bottom of baking dish with spinach.

3.  Spread on cream cheese mixture.

4.  Sprinkle parmesan cheese on top

5.  Bake at 375 for 40 minutes.
