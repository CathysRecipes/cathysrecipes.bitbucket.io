.. stuffed-cabbage:

Stuffed Cabbage
================

.. author:: Joan
.. categories:: side dishes
.. tags:: 
.. comments::

=========  ==================================================
measure    ingredients
=========  ==================================================
1          medium cabbage
1 1/4 LB   ground beef
1 cup      boiled rice
1/2 cup    chopped onion (sautéed in butter)
1 1/2 tsp  salt
1/2 tsp    pepper
2 tbls     butter for frying
1 cup      water or beef bullion (save for middle of recipe)
=========  ==================================================

Recipe
------

1.  Cut off outside leaves and remove the core of the cabbage head.

2.  Place it upside down in a pot and pour enough boiling water to fill and cover the cabbage.

3.  Boil for 10 minutes. Then drain and detach leaves.

4.  Mix the fillings thoroughly. 

5.  Pat 1-2 tbls each on the thick part of the leaf.  Roll and close with a toothpick.

6.  Heat butter in a frying pan, and fry each roll lightly.

7.  Put all of them in a saucepan and pour the bullion over them. Cover and bring to a boil.

8.  Reduce and simmer for 1 hour
