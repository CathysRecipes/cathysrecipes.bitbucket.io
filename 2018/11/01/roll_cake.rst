.. roll-cake:

Chocolate Roll
==============

.. author:: Mom
.. categories:: desserts
.. tags:: cake
.. comments::

=========  =======================================
measure    ingredients
=========  =======================================
`cake`
5          eggs
3/4 cup    sugar
2 Tbsp     coco powder
1 Tbsp     flour
2 tsp      vanilla extract
`filling`
1 	       small container heavy cream
1-2 tsp    sugar
Handful    strawberries
=========  =======================================

Recipe - for cake roll
----------------------

1.  Separate egg whites.  Beat until fluffy and foamy.

2.  Mix together everything but egg whites.  After mixed, add foamy egg whites.

3.  Use 10x5 cookie sheet.  Cover it with parchment paper.  Spray with Pam, pour mixture onto cookie sheet, and bake at 350 degrees for 25 minutes.

4. Lay cake flat on a non furry towel and peel parchment paper off.  Let cool and then roll up cake in towel.

Recipe - for filling
--------------------

1.  Beat heavy cream.  Add 1-2 tsp of sugar (to taste).

2.  Unroll cake and coat with a layer of filling.

3.  Add pieces of cut up strawberries.

4.  Roll cake.  Sprinkle confectioner sugar on top before serving.

