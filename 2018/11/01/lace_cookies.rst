.. lace-cookies:

Lace Cookies
============

.. author:: Mom
.. categories:: christmas cookies
.. tags:: christmas, cookies, dessert
.. comments:: 

=========  =======================================
measure    ingredients
=========  =======================================
1 cup      quick (instant quaker oats)
1 cup 	   sugar
1 	       stick butter, melted
1 	       egg
1 1/2 tsp  orange juice
1 1/2 tsp  vanilla extract
1          package chocolate chips
=========  =======================================

Recipe
------

1.  In a large bowl, combine sugar and butter.  Mix in beaten egg, orange juice, and vanilla extract.

2.  Drop 1/2 tsp size amounts of mixture onto foil lined cookie sheet 2 inches apart.

3.  Bake @ 375 degrees for 8-10 minutes until dark brown.

4.  Remove foil sheet and allow cookies to cool before peeling them off.

5.  Melt pack of chocolate chips, drizzle on to cookie.  Make cookie sandwich with similar sized pieces. 

.. note::
   * mom usually doubles the recipe for lots of lace