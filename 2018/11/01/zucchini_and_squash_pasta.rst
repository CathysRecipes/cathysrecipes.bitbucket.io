.. zucchini-and-squash-pasta:

Zucchini and Squash Pasta with Sweet Italian Sausage and Pappardelle
====================================================================

.. author:: foodnetwork.com
.. categories:: dinners
.. tags:: 
.. comments::


This recipe was adapted from `Emril Lagasse <http://www.foodnetwork.com/recipes/emeril-lagasse/zucchini-and-squash-pasta-with-sweet-italian-sausage-and-pappardelle-recipe/reviews/index.html>`_

==========  =======================================
measure     ingredients
==========  =======================================
4 Tbsp	    extra virgin olive oil
1 lb	    sweet italian sausage, no casings
1 lb	    zucchini, cut into half-moons
1 lb	    summer squash, cut into half-moons
1	        onion, thinly sliced
1/4 cup     diced red pepper
1/4 cup     chopped fresh basil leaves, plus sprigs, for garnish
`to taste`  salt and pepper
1 lb        fresh pappardelle (or pasta)
--          essence, for garnish
==========  =======================================


Recipe
------

1.  In a large stockpot over high heat, bring 4 quarts of salted water to a boil.  Add 2 tablespoons olive oil.  

2.  In a large saute pan over medium high heat, add remaining 2 tablespoons olive oil and sausage.  Cook until sausage is browned, about 6 minutes.  Add zucchini, squash, onions, red pepper, and 1/4 cup chopped basil and cook for 4-5 minutes, or until the squash has begun to caramelize.  Season with salt and pepper. 

3.  Add pasta to the boiling water and cook until just al dente, about 1 minute.  Drain reserving 1/2 cup pasta water.  

4.  Pour cooked pasta and 1/2 cup pasta water into the sausage sauce.  Garnish with basil sprigs and Essence.
