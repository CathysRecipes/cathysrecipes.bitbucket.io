.. apple-salsa:

Apple Salsa
===========

.. author:: Mom
.. categories:: appetizers
.. tags:: chopping
.. comments::

=========  =======================================
measure    ingredients
=========  =======================================
4	       granny smith apples, peeled and chopped
1/2 	   onion, finely chopped
2 Tbsp     white vinegar
1/2 cup    sugar
1 tsp      dry mustard
1 tsp      cumin
1 or 2     jalapeno peppers, chopped
dash	   salt
dash 	   pepper
splash     lemon juice
=========  =======================================

Recipe
------

1.  Combine all ingredients and serve.

