.. mint-cookies:

Mint Cookies
============

.. author:: Mom
.. categories:: christmas cookies
.. tags:: christmas, cookies, dessert
.. comments:: 

=========  ============================================
measure    ingredients
=========  ============================================
`cake`
1 cup	   all purpose flour
1 cup 	   sugar
1/2 cup    butter/margarine softened
4          eggs
16oz can   hershey's syrup 
`mint`        
2 cups     confectioner sugar
1/2 cup    butter softened
1 Tbsp     water
1/2 tsp    mint extract
3 drops    green food coloring
6 Tbsp     butter
1 cup      hershey's mint chocolate chips or semi-sweet
=========  ============================================

Recipe - Step 1 - cake layer
-----------------------------

1.  Heat oven to 350 degrees.  Grease rectangular pan 13x9

2.  In large bowl, beat flour, sugar, butter, eggs and syrup until smooth.

3.  Pour mixture into prepared pan, bake 25-30 minutes or until top springs.  Cool completely.  

Recipe - Step 2 - mint layer
-----------------------------

1.  In a small bowl, combine confectioner sugar, butter, water, mint extract, and food coloring.  Beat until smooth.  Ice the cake layer and chill until firm.

Recipe - Step 3 - topping
-----------------------------

1.  In a small bowl, melt butter and chocolate chips on high for 1 - 1 1/2 minutes or until chips are melted and mixture is smooth when stirred.  Ice the mint layer, refrigerate until firm and cut into squares.



.. note::
   * this cookie is a 3 step process - ingredients are listed by step as they appear in recipe section