.. gingerbread-cookies:

Gingerbread Cookies
====================

.. author:: Mom
.. categories:: christmas cookies
.. tags:: christmas, cookies, dessert
.. comments::

=========  =======================================
measure    ingredients
=========  =======================================
1 cup      margarine, melted
1 1/2 cup  sugar
1 	       egg
4 tsp      grated orange peel 
2 Tbsp     dark corn syrup
3 cups     sifted flour
2 tsp      baking soda
2 tsp      ground cinnamon 
2 tsp      ground ginger
1/2 tsp    ground cloves
1/2 tsp    salt
=========  =======================================

Recipe
------

1.  Mix all ingredients in a bowl.   Cover counters and rolling pin with flour and roll out dough.  

2.  Cut out with cookie cutter and bake at 375 degrees until browned.

3.  Continue until all dough is used.


.. note::
   * If mixture is too sticky, add small amounts of flour until it is easily rolled out
   * If mixture is too dry and crumbles, add small amounts of water