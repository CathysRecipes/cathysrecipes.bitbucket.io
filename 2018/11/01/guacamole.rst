.. guacamole:

Guacamole
=========

.. author:: Kelly
.. categories:: appetizers 
.. tags:: quick, easy, spicy
.. comments::

=========  =======================================
measure    ingredients
=========  =======================================
2 	       avocado
1   	   small red onion (about 1/2 cup)
2 Tbsp     cilantro
1      	   lemon
1 	       lime
1/2 tsp    salt
dash       pepper
1/2 tsp    cayanne
1/2 tsp    cumin
=========  =======================================

Recipe
------

1.  Cut avocado in 1/2, seed and mash.

2.  Add in all other ingredients.

3.  Mix, mash, and serve.


.. note::
   * It will change color to a dark green/brown if you keep it open or for more than a few hours..it's still good to eat.