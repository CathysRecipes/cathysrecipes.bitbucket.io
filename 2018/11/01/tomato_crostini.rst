.. tomato-crostini-with-whipped-feta:

Tomato Crostini with Whipped Feta
=================================

.. author:: oprah.com
.. categories:: appetizers
.. tags:: 
.. comments::

This recipe was adapted from `Oprah <http://www.oprah.com/food/Tomato-Crostini-with-Whipped-Feta-Recipe-Ina-Garten>`_


=========  =======================================
measure    ingredients
=========  =======================================
6 oz       feta cheese, crumbled
2 oz       cream cheese, at room temperature
2/3 cup    olive oil, divided
2 Tbsp	   freshly squeezed lemon juice
`totaste`  kosher salt
->         freshly ground black pepper
2 Tbsp     pine nuts
2 Tbsp     minced shallots (2 shallots)
2 tsp	   minced garlic (2 cloves)
2 Tbsp     red wine vinegar
2 lbs 	   ripe heirloom tomatoes, 1/2 inch diced
3 Tbsp     julienned fresh basil leaves, plus extra for serving
20-25 	   1/2 inch thick diagonal baguette slices, toasted
=========  =======================================

Recipe
------

1.  Place feta and cream cheese in the bowl of a food processor fitted with a steel blade.  Pulse until cheeses are mixed.  Add  1/3 cup olive oil, lemon juice, 1/2 tsp salt, and 1/4 tsp pepper and process until smooth.  Refrigerate until ready to serve.

2.  Place pin nuts in a dry saute pan over low heat and cook 5 to 10 minutes, stirring often, until lightly browned.  Set aside.

3.  For the tomato topping, up to an hour before serving, combine shallots, garlic, and vinegar in a medium bowl.  Set aside 5 minutes to allow shallots to absorb vinegar.  Whisk in remaining 1/2 tsp pepper.  Add tomatoes, stir gently, and set aside 10 minutes.  Stir in basil and taste for seasoning.

4.  To assemble crostini, spread each baguette slice with a generous amount of whipped feta.  With a slotted spoon , place tomatoes on top.  Put crostini on plates and scatter with the reserved toasted pine nuts.  Sprinkle with basil and serve.

