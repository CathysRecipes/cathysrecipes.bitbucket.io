.. chocolate-chery-thumbprints:

Chocolate Cherry Thumbprints
=============================

.. author:: Mom
.. categories:: christmas cookies
.. tags:: christmas, cookies, dessert
.. comments::

===========  ===========================================
measure      ingredients
===========  ===========================================
3/4 cup	     sugar
2 	         eggs
2/3 cup      butter softened
1 tsp	     vanilla extract
12 oz	     semi-sweet chocolate chips, divided
2 cups       quaker oats, uncooked
1 1/2 cups   all purpose flour
1 tsp        baking powder
1/4 tsp      salt (optional)
20 oz        maraschino cherries, drained and patted dry
===========  ===========================================


Recipe
------

1.  Heat over to 350 degrees.  Beat sugar, butter, eggs, and vanilla until smooth.

2.  Add 1 cup chocolate chips, melted; mix well.

3.  Stir in oats, flour, baking powder, and salt.  Mix well.  

4.  Cover and chill dough for 1 hour.

5.  Shape dough into 1 inch balls.  Place 2 inches apart on un-greased cookie sheet.

6.  Press deep centers with thumb.  Place cherry into centers

7.  Bake 10-12 minutes until set.  Remove to wire rack, cook completely.  

8.  Drizzle cookies with remaining 1 cup chocolate chips, melted.


 