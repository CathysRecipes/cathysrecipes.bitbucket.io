.. tomato-basil:

Tomato, Basil, and Fresh Mozzarella
===================================

.. author:: Mom
.. categories:: appetizers
.. tags:: quick, easy
.. comments::

===========  =======================================
measure      ingredients
===========  =======================================
4 	         medium ripe tomatoes
8 oz 	     ball of fresh mozzarella
1            bunch basil
1 	         small clove of garlic, minced
1            chopped green onion
`to taste`   kosher salt
->	         ground black pepper, to taste
..	         extra virgin olive oil
..	         balsamic vinegar
===========  =======================================

Recipe
------

1.  Slice tomatoes and cheese, arrange on a flat plate, tomatoes first then cheese on top.

2.  Place basil leaves on and around tomatoes and cheese.

3.  Season with salt and pepper.  Top with garlic and green onion.  Drizzle with olive oil and vinegar.

