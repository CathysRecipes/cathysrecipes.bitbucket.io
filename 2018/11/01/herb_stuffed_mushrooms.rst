.. herb-stuffed-mushrooms:

Herb Stuffed Mushrooms
======================

.. author:: Mom
.. categories:: appetizers
.. tags:: 
.. comments::

=========  =======================================
measure    ingredients
=========  =======================================
2 oz 	   chopped cooked ham
1 tsp 	   oregano
1/4 tsp    thyme
1 tsp      chopped parsley
2 Tbsp     grated parmesan 
2 Tbsp     bread crumbs
`to tase`  olive oil
dash       salt
dash       pepper	   
=========  =======================================

Recipe
------

1.  Wash mushrooms and remove stalks.  Trim stalks as necessary and finely chop.

2.  Mix chopped stalks with ham, herbs, grated cheese, and bread crumbs.  Season to taste.

3.  Put mushrooms in baking dish and fill with mixture.

4.  Pour a little olive oil on each.

5.  Cover and cook at 350 degrees for 20-25 minutes.
 