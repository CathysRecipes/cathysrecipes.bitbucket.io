.. banana-nut-snacks:

Banana Nut Snacks
=================

.. author:: Mom
.. categories:: desserts
.. tags:: dessert, snack
.. comments::

=========  =======================================
measure    ingredients
=========  =======================================
2	       very ripe bananas
1/2 cup	   unsalted butter, softened
1 1/2 cup  packed brown sugar
2          large eggs
1 tsp      vanilla extract
2 cups     all purpose flour
1/2 tsp    salt
2 tsp      baking powder
1 cup      chopped walnuts
1/2 cup    mini chocolate chips
3/4 cup    confectioner sugar
3 1/2 tsp  water
1 tsp      unsweetened cocoa powder
=========  =======================================

Recipe
------

1.  Heat oven to 350 degrees.  Line 13x9 baking pan with foil.  Coat foil with nonstick cooking spray.

2.  In large bowl, beat bananas and butter.  Beat in sugar until smooth.  Beat in eggs, 1 at a time.  Stir in vanilla, flour, salt, baking powder, walnuts, and chocolate chips.  Spread into pan.

3.  Bake at 350 degrees until top is dry to the touch (about 40 minutes).  Cool in pan on wire rack for 15 minutes.  Remove from pan and transfer back to rack.  Let cool.


.. note::
   * Makes 24 