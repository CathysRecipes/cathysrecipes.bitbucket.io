.. swedish-meatballs:

Swedish Meatballs
=================

.. author:: Mom
.. categories:: appetizers
.. tags:: 
.. comments::

=========  =======================================
measure    ingredients
=========  =======================================
1 	       large container of grape jelly
1          container medium taco sauce
=========  =======================================

Recipe
------

1.  Melt together jelly and sauce in pot.

2.  Add meatballs to melted sauce.

3.  Serve meatballs warm.

