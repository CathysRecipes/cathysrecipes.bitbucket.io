.. _holiday-thanksgiving-SausageStuffing:

Sausage Stuffing
================

.. warning::
   This requires multiple days of preperations (~3 days to make bread stale).

==============  =======================================
measure         ingredients
==============  =======================================
1 loaf          bread (multi-grain)
1 lb            sausage, ground
1/2             onion, chopped
3 stalks        celery
1/2 tsp         rosemary
1/2 tsp         sage
1/2 tsp         thyme
~1/2 cup        chicken stock
`optional`      apples
...             raisins
==============  =======================================

Recipe
-------

1. Cut bread in cubes and leave out for a few days.
2. Brown sausage in frying pan. Set aside.
3. Use the sausage drippings to saute onion and celery.
4. Sprinkle rosemary, sage and thyme onto stale bread
5. Add sausage, onions, celery, salt and pepper. (And apples and raisins if using).
6. Spray PAM on all sides of the pan.
7. Add stuffing.
8. Cook at 350 degrees for about 1 hour.
9. When bastin the turkey, add some drippings to the stuffing.

.. note::
   * Stuffing can be cooked in the turkey.
   * Can make and refrigerate sausage, onions, celery the night before.
   * Chicken stock should be added as needed to moisten.
   * Most importantly, note: **People LOVE this dish!**

   