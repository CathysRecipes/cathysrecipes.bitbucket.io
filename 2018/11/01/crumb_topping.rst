.. crumb-topping:

Crumb Topping (for apple pie)
=============================

.. author:: Mom
.. categories:: desserts
.. tags:: dessert, pie
.. comments::

=========  =======================================
measure    ingredients
=========  =======================================
1 cup 	   flour
1/2 cup    brown sugar
1/2 tsp    cinnamon
1/2 cup    butter
=========  =======================================

Recipe
------

1.  Mix dry ingredients.

2.  Cream in butter.

3.  Add to pie

