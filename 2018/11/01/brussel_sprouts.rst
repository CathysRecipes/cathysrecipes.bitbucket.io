.. brussel-sprout-saute:

Brussel Sprout Saute
====================

.. author:: Mom
.. categories:: side dishes
.. tags:: 
.. comments::

=========  =======================================
measure    ingredients
=========  =======================================
20 oz	   brussel sprouts (2 containers)
3 Tbsp     salad oil
2 Tbsp     dijon mustard
1/4 tsp    salt
1/4 cup    water
=========  =======================================

Recipe
------

1.  Cut each brussel sprout, length-wise, in half.  

2.  In 5 quart dutch oven, over high heat, in hot oil, cook brussel sprouts.  Stir frequently for 5 minutes.  

3.  Add mustard, salt, and water.  Reduce heat to medium; cover and cook until sprouts are tender-crisp, about 5 minutes.  Stir occasionally.
