.. rainbow-cookies:

Rainbow Cookies
================

.. author:: Mom
.. categories:: christmas cookies
.. tags:: christmas, cookies, dessert
.. comments:: 

=========  =======================================
measure    ingredients
=========  =======================================
7-oz	   can almond paste
12-oz	   jar apricot preserves
1 cup	   sugar
3 sticks   margarine melted
4	       eggs separated
1 tsp 	   almond extract
2 cups	   sifted flour
1/4 tsp    salt
10 drops   green food coloring
8 drops    red food coloring
4 squares  semi-sweet chocolate
=========  =======================================

Recipe
------

1.  Grease pan and line with wax paper.

2.  Break up paste with fork, add margarine, sugar, yolk, and extract.  Beat 5 minutes.

3.  Add flour and salt.

4.  Beat egg whites in a small bowl.

5.  Fold into mixture.

6.  Remove 1 1/2 cups of batter, add green food coloring and bake.

7.  Removed another 1 1/2 cups and add red food coloring and bake.  Bake the remaining mixture with no food coloring.

8.  Remove from pan once cooked and allow to cool.

9.  Heat apricot preserves and strain.  Pour half of the reserves on top of green layer.  Place plain layer on top and add the remaining reserves.  Place the red layer on top.

10. Place heavy board on top of all 3 layers and refrigerate overnight.

11. Melt chocolate and coat top layer.  Refrigerate until hard then cut into small pieces.   


.. note::
   * bake each layer at 350 degrees for  15 minutes