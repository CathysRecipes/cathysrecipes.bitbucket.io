.. crumb-cake:

Crumb Cake
==========

.. author:: Mom
.. categories:: desserts
.. tags:: cake
.. comments::

=========  =======================================
measure    ingredients
=========  =======================================
1 box	   pillsbury moist supreme butter recipe
`topping`
4 cups	   flour
1 1/3 cup  sugar
3 tsp      cinnamon
3 sticks   butter
1.5 tsp    vanilla extract
1.5 tsp    almond extract
=========  =======================================

Recipe - cake portion
---------------------

1.  Make cake and bake according to instructions on package.  

2.  Remove cake 10 minutes prior to its finish time and sprinkle with topping and bake for the remaining 10 minutes.

Recipe - topping
----------------

1.  Combine flour, sugar, and cinnamon in bowl.

2.  In separate bowl, melt butter and add vanilla and almond extract.

3.  Mix flour and butter mixture together.  It will be moist clumps.

.. note::
   * Cake needs to be cooked and spring back to the touch before topping is added.  If it is too wet, cake will not finish cooking.
   * Once topping is added, gently press it down so it sticks to the top of the cake.