.. artichoke-quiche:

Artichoke Quiche
=================

.. author:: Mom
.. categories:: appetizers
.. tags:: quick and easy
.. comments::

==========  =======================================
measure     ingredients
==========  =======================================
1           pie shell
1 cup       mozzarella cheese
1/2 can     artichoke hearts
1/2 cup     pepperoni
1/4 cup     grated cheese
4           eggs
`to taste`  salt
            pepper
==========  =======================================

Recipe
------

1.  Cut cheese and meat into small chunks.

2.  Mix all ingredients and place in the pie shell.

3.  Cook at 350 for 45 minutes.
