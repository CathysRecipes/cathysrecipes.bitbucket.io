.. apple-pie:

Apple Pie
=========

.. author:: Mom
.. categories:: desserts
.. tags:: pie, dessert
.. comments:: 

=========  =======================================
measure    ingredients
=========  =======================================
3/4 cup	   sugar
1/4 cup    all purpose flour
1/2 tsp    ground nutmeg
1/2 tsp    ground cinnamon
dash 	   salt
6	       medium apples (peeled & thin sliced) (red delicious, recommended)
2 Tbsp 	   butter or margarine
=========  =======================================

Recipe
------

1.  Heat oven to 425 degrees.

2.  Mix sugar, flour, nutmeg, cinnamon, and salt.  Stir in apples.  

3.  Turn into pastry lined pie plate, dot with butter/margarine.

4.  Bake for 40-50 minutes.  You may need to cover with foil for the last 10 minutes if crust is browning too much.

.. note::
   * Recipe for a 9 inch pie crust