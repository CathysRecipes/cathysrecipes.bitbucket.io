.. popovers:

Popovers
============

.. author:: Mom
.. categories:: side dishes
.. tags:: 
.. comments::

=========  =======================================
measure    ingredients
=========  =======================================
1 cup      milk
1 Tbsp     salad oil
2          eggs
1 cup      all purpose flour, sifted
1/2 tsp    salt
=========  =======================================

Recipe
------

1.  Preheat oven to 425 degrees.  

2.  Use wire whisk to mix milk, eggs and oil until well blended.  

3.  In a separate bowl, combine salt and flour.

4.  Add 1/2 of the dry ingredients to liquid and beat.  Add remaining 1/2 and mix until blended, but lumpy.

5.  Grease large muffin tins.  Fill tins to the top with batter.  

6.  Bake 20 minutes.  Reduce temperature to 350 degrees and bake for 35 minutes.  Do not ope oven door when temperature is reduced.
