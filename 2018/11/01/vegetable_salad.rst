.. marinated-vegetable-salad:

Marinated Vegetable Salad
=========================

.. author:: people magazine
.. categories:: dinners
.. tags::
.. comments::



=========  ==============================================
measure    ingredients
=========  ==============================================
1/2 cup    olive oil
1/4 cup    white wine vinegar
1 tbsp     italian seasoning
1 tbsp     dijon mustard
3/4 tsp    salt
1/2 tsp    sugar
1/2 tsp    ground black pepper
3 cups     cauliflower florets
2 cups     cherry or grape tomatoes, halved
3          medium zucchini, cut into bite size pieces
3          carrots, peeled and cut into bite-sized pieces
1          small onion, thinly sliced
=========  ==============================================

Recipe
------

1.  In a small bowl, whisk together the olive oil, vinegar, italian seasoning, dijon mustard, garlic, salt, sugar, and pepper.

2.  In a large bowl, combine the cauliflower, tomatoes, cherry peppers, zucchini, carrots, and onion.  Pour the dressing over the vegetables, tossing gently to coat.  Cover and refrigerate for at least 2 hours or p to 24 hours, stirring occasionally.  Serve with a slotted spoon.  
