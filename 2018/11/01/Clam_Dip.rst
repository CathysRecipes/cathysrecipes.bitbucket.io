.. clam-dip:

Clam Dip
============

.. author:: Robin
.. categories:: appetizers
.. tags:: 
.. comments::

=========  =======================================
measure    ingredients
=========  =======================================
2 8oz can  chopped thumbs
1/2 cup    bread crumbs
1 tsp      lemon juice
1 minced   onion
1 minced   garlic
1/2        green pepper,chopped
1/4 lb     lettuce
1 dash     tabasco
1 dash     pepper
1 tsp      oregano 
=========  =======================================

Recipe
------

1.  Simmer clams (in their own juice) and lemon juice for 15 minutes.

2.  Sauté everything except bread crumbs and cheese.

3.  Mix clams and mixture. Add bread and sprinkle with parmesan cheese.

4.  Bake uncovered at 350 for 20-30 minutes.
