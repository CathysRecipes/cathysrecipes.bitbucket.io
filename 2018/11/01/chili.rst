Chili
=====

.. author:: Laura
.. categories:: dinners
.. tags:: 
.. comments::

=========  =======================================
measure    ingredients
=========  =======================================
3 1/2 lbs  ground beef
2 tsp	   salt
1 1/2 tsp  pepper
1 1/2 tsp  cumin
3          1 pound cans kidney beans
3          1 pound cans tomatoes
3          green peppers, seeded and chopped
2 Tbsp     vegetable oil
5 cups     chopped onion
3          cloves of garlic, minced
1/2 cup	   minced parsley
1/4-1/3 c  chili powder
=========  =======================================

Recipe
------

1.  Prepare vegetables.  

2.  In large skillet, brown ground beef and pour off fat.  Put in a large pot.  

3.  Saute green peppers in oil for 5 minutes.  Add onions and cook until soft, stirring frequently.  Add garlic and parsley and add to beef mixture.

4.  Stir in chili powder and cook 10 minutes.  Add tomatoes, salt, pepper, and cumin.  Cover and simmer 1 hour.

5.  Remove cover and cook 30 minutes

.. note::
   * freezes well 