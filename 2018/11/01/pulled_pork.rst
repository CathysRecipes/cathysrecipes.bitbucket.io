.. slow-cooked-pulled-pork:

Slow-Cooked Pulled Pork
=======================

.. author:: good housekeeping
.. categories:: dinners
.. tags:: 
.. comments::

This recipe was adapted from `Good Housekeeping <http://www.goodhousekeeping.com/recipefinder/slow-cooked-pulled-pork-1100>`_

=========  =======================================
measure    ingredients
=========  =======================================
1          medium onion, chopped
1/2 cup    ketchup
1/3 cup    cider vinegar
1/4 cup    packed brown sugar
1/4 cup    tomato paste
2 Tbsp     sweet paprika
2 Tbsp     worcestershire sauce
2 Tbsp     yellow mustard
1 1/2 tsp  salt
1 1/4 tsp  ground black pepper
4 lbs  	   boneless pork shoulder blade roast (pork butt) cut in 4 pieces
12         soft sandwich buns, warmed
--         dill pickles (optional)
--         potato chips (optional)
--         hot sauce (optional)
=========  =======================================

Recipe
------

1.  In 4 1/2-6 quart slow cooker pot, stir onion, ketchup, vinegar, brown sugar, tomato paste, paprika, worcestershire, mustard, salt, and pepper until combined.  Add pork to sauce mixture and turn to coat well with sauce.

2.  Cover slow cooker with lid and cook pork mixture on low setting s manufacturer directs, 8-10 hours or until pork is very tender.

3.  With tongs, transfer pork to a large bowl.  Turn setting on slow cooker to high; cover and heat sauce to boiling to thicken and reduce slightly.  

4.  While sauce boils, with 2 forks, pull pork into shreds.  Return shredded pork to slow cooker and toss with sauce to combine.  Cover slow cooker and heat through on high setting if necessary.

5.  Spoon pork mixture onto bottom of sandwich buns; replace tops of buns.  Serve sandwiches with pickles, potato chips, and hot sauce if you like.
