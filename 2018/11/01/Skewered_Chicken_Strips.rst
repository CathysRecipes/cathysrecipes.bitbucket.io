.. skewered-chicken-strips:

Skewered Chicken Strips
=======================

.. author:: Mom
.. categories:: dinners
.. tags:: yummy
.. comments::

=========  =======================================
measure    ingredients
=========  =======================================
2 tbs      honey
1/4 cup    orange juice
1/4 cup    soy sauce
2 tbs      peanut butter
1/4 cup    cooking wine
--         chicken breasts cut in long strips and
--         skewered on bamboo sticks
=========  =======================================

Recipe
------

1.  Microwave for one minute then whisk until blended.

2.  Use about 1/3 of mixture to marinate and the rest for dipping.

3.  Grill or broil strips.
