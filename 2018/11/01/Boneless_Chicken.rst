.. boneless-chicken-thighs

Boneless Chicken Thighs
=======================

.. author:: Mom
.. categories:: dinners
.. tags:: 
.. comments::

=========  =======================================
measure    ingredients
=========  =======================================
1 cup      orange juice
1 pkg      lipton onion soup
1 tsp      soy sauce
1 tsp      garlic powder
1 pkg      boneless chicken thighs
=========  =======================================

Recipe
------

1. Mix together and bake at 350 for 1 hour.