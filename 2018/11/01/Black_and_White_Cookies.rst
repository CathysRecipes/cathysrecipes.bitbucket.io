.. black-and-white-cookies:

Black and White Cookies
=======================

.. author:: Katrina
.. categories:: desserts
.. tags:: easy
.. comments::

=========  =======================================
measure    ingredients
=========  =======================================
`cookies`
1 pkg      yellow or white cake mix
2          eggs
1/2 cup    cooking oil
`icing`
2 1/4 cup  confectioners sugar
1/2 tsp    vanilla
3 tbsp     milk
1 tbsp     cocoa powder
1 tbsp     vegetable oil
=========  =======================================

Recipe
------

1.  Preheat oven to 350.

2.  In a large bowl mix together the cake mix, eggs and oil until well blended.

3.  Drop batter by large spoonfuls onto ungreased baking sheet.  

4.  Place in oven and bake for 12-15 minutes or until lightly browned.

5.  Let cookies cool for a few minutes on the baking sheet, then remove to cooking rack to cool completely.

6.  Spread the flat part of the cookies with icing.

7.  Icing-In a medium bowl, blend the confectioners sugar, vanilla and milk together. Put half the mixture in another bowl and add the coco and oil.
