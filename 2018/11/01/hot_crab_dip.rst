.. hot-crab-dip:

Hot Crab Dip
============

.. author:: Kelly 
.. categories:: appetizers
.. tags:: 
.. comments::

=========  =======================================
measure    ingredients
=========  =======================================
8 oz 	   sour cream
16 oz      cream cheese
4 Tbsp     mayonnaise
1 tsp      lemon juice
1 tsp      dry mustard
pinch      garlic salt
8 oz       cheddar cheese
16 oz      crab meat
--         old bay to taste
=========  =======================================

Recipe
------

1.  Combine all ingredients except crab and cheese.  Mix together thoroughly until smooth.

2.  Mix in crab meat and cheese with spatula.  

3.  Cook in casserole dish at 375 degrees for 35 minutes.  


.. note::
   * Serve with crackers or soft pretzels.