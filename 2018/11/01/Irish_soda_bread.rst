.. irish-soda-bread:

Irish Soda Bread
================

.. author:: Mom
.. categories:: bread
.. tags:: 
.. comments:: 

=========  =======================================
measure    ingredients
=========  =======================================
4 cups     flour
1 tsp      salt
2 Tbsp     caraway seeds
2 cups     raisin
1 1/2 cup  buttermilk
1/4 cup    sugar
1 tsp 	   baking powder
1/4 cup    butter
1          egg
1 tsp      baking soda
=========  =======================================

Recipe
------

1.  Heat oven to 375 degrees.  

2.  Sift flour, sugar, salt, and baking powder.  Add caraway seeds. 

3.  Cut in butter with knife or pastry blender until mixture is crumbly.  

4.  Add raisin in separate bowl, combine egg and buttermilk.  Add baking soda to liquid (it will bubble).  Quickly stir in dry ingredients until a moist dough is formed.

5.  Shape dough on a floured surface.  Place on a greased baking sheet.  Cut a deep cross.  

6.  Bake 1 hour or until a tester comes out clean.  Cook 10-15 minutes.


.. note::
   * If dough feels stiff, add more buttermilk.
   * Soak raisins before baking