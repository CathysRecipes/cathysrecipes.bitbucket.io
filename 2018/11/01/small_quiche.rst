.. small-quiche:

Small Quiche
============

.. author:: Mom
.. categories:: appetizers
.. tags:: 
.. comments::

==========  =======================================
measure     ingredients
==========  =======================================
`pastry`
1 pound     butter
1 pound     cream cheese
4 cups      flour
`filling`
1/4 pound   swiss cheese, grated
3           eggs, beaten
1 cup       heavy cream
1/2 cup     milk
1/2 tsp     salt
1/2 tsp     dry mustard
1/8 tsp     pepper
1/2 pound   cooked bacon, crumbled
==========  =======================================

Recipe - cream cheese pastry
----------------------------

1. Mix together butter, cream cheese, and flour.  Line mini cupcake pan with a very thin layer of mixture.

Recipe - filling
----------------

1.  Mix together swiss cheese, eggs, heavy cream, milk, salt, dry mustard, and pepper.  

2.  Pour into pastry lined cupcake pan.

3.  Top each with a pinch of crumbled bacon.

4.  Bake at 400 degrees for 8 minutes.  Reduce to 350 degrees for 15 minutes.
 
.. note::
   * These can be frozen and reheated.