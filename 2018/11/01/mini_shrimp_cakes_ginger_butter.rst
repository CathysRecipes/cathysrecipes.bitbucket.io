Mini Shrimp Cakes with Ginger Butter
====================================

.. author:: oprah.com
.. categories:: appetizers
.. tags:: 
.. comments::

This recipe adapted from `Giada <http://www.foodnetwork.com/recipes/giada-de-laurentiis/mini-shrimp-cakes-with-ginger-butter-recipe/index.html>`_

**Prep Time:** 25 min
**Inactive Prep Time:** 30 min
**Cook Time:** 30 min
**Level:** Easy
**Serves:** 15 shrimp cakes

===============  =======================================
measure          ingredients
===============  =======================================
`shrimp cakes`
3 Tbsp           vegetable olive oil
5                large button mushrooms, stemmed,
->               chopped into 1/4-inch pieces (about 4 ounces)
2                large shallots, minced
1                medium carrot, diced into 1/4-inch pieces
1 lb             extra-large shrimp, peeled and deveined
3 Tbsp           plain breadcrumbs
1                large egg
--               Zest of 1/2 large lemon
1 tsp            kosher salt
1/2 tsp          freshly ground black pepper
`ginger butter`
1 stick          unsalted butter, at room temperature
1                1/2-inch-long fresh gingerroot, peeled and finely grated (about 4 tsp)
2 tsp            soy sauce
1/4 cup          vegetable oil, plus extra as needed
===============  =======================================

Recipe - shrimp cakes
----------------------

1. Heat the vegetable oil in a 12-inch nonstick skillet over medium-high heat. 
2. Add the mushrooms, shallots and carrots. 
3. Cook, stirring frequently, until the carrots begins to soften, 6 to 7 minutes. 
4. Transfer the mixture to a food processor. 
5. Add the shrimp, breadcrumbs, egg, lemon zest, salt and pepper. 
6. Pulse until combined but still chunky. Form the mixture into 15 patties, each 2 inches in diameter and 1/2-inch thick, using damp hands. 
7. Place on a plastic wrap-lined baking sheet and refrigerate for 30 minutes.

Recipe - ginger butter
-----------------------

1. Mix together the butter, ginger and soy sauce in a medium bowl, using a rubber spatula, until combined. 
2. Place the mixture on a piece of plastic wrap. 
3. Fold the plastic wrap over and form the mixture into a log about 7 inches long and 1-inch in diameter. 
4. Refrigerate until firm, at least 30 minutes. 
5. When ready to serve, slice the butter into 1/4-inch-thick pieces.

Recipe - end
-------------

1. Heat the vegetable oil in a 12-inch nonstick skillet over medium-high heat. 
2. Add half of the patties and cook until golden, 3 to 4 minutes per side. 
3. Cook the remaining patties, adding extra oil if needed. 
4. Serve the shrimp cakes while still warm with a slice of ginger butter on top.

.. note:: Use any leftover ginger butter to top chicken or fish.