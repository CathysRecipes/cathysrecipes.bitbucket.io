.. chocolate-chip-cookies:

Chocolate Chip Cookies
======================

.. author:: Dad
.. categories:: christmas cookies
.. tags:: christmas, cookies, dessert
.. comments::

=========  =======================================
measure    ingredients
=========  =======================================
2 1/4 cup  all purpose flour
1 tsp 	   baking soda
1 tsp      salt 
1 cup	   butter, softened
3/4 cup    granulated sugar
3/4 cup    packed brown sugar
1 tsp      vanilla extract
2 	       large eggs
2 cups     semi-sweet chocolate chips
=========  =======================================

Recipe
------

1.  Preheat oven to 375 degrees.

2.  Combine flour, baking soda, and salt in small bowl.

3.  Beat butter, granulated sugar, brown sugar, and vanilla extract in large mixer bowl until creamy.  Add eggs 1 at a time, beating well after each addition.  Gradually beat in flour mixture.

4.  Stir in chocolate chips.

5.  Drop by rounded tablespoon onto un-greased cookie sheet.

6.  Bake for 9-11 minutes


.. note::
   * makes about 5 dozen