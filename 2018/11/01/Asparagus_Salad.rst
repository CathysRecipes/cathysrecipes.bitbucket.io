.. asparagus-salad:

Asparagus Salad
================

.. author:: Mom
.. categories:: side dishes
.. tags:: quick, easy
.. comments::

==========  =======================================
measure     ingredients
==========  =======================================
1 1/2 lb    asparagus cut in 1" pieces
2 medium    tomatoes, cut in chunks
1           yellow pepper cut in "
1 (71/2g)   can artichokes, drained, cut in wedges
`dressing`
1/3 cup     olive oil
1/2         juice from a lemon
2 tbls      tarragon vinegar
3 tbls      grated parmesan
1/4 tsp     salt
1/8 tsp     pepper
1/8 tsp     sugar
==========  =======================================

Recipe
------

1.  Steam asparagus 5-10 minutes until tender.

2.  Rinse in cold water.

3.  Toss together asparagus, tomatoes, peppers and artichokes.

4.  Cover and chill for 30 minutes.

5.  Whisk together oil. lemon juice, vinegar, cheese, salt, pepper and sugar in a small bowl.  

6.  Pour over asparagus and toss.


.. note::
   * 